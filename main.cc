
#include "matrix.h"
#include "vector.h"
#include <iostream>
#include<time.h>
#include <cstdlib>
#include <pthread.h>

#define MV
using namespace std;


int main()
 {

  int x=4;
  int y=4;
  int w=4;
  pthread_t threads[16];
  int rc1;
  int rc2;
  int z=0;
  
#ifdef MV

  Matrix matrix1(x,y);
  Vector vector2(w);

  struct mv args;
  args.m1= &matrix1;
  args.m2= &vector2;

  int i;
  for(i=0;i<16;i++)
    {
	  args.id=i;
      if((pthread_create(&threads[i], NULL , mult1,(void *) &args))!=0)
	return(-1);
    }

  for(i=0;i<16;i++)
    {
      pthread_join(threads[i], NULL);
    }

  
/*for(int i=0; i<v3->m; i++)
    {
      cout<<v3->array[i]<<endl;
      }
  */
#else
  Matrix matrix1(x,y);
  Matrix matrix2(x,y);

  struct mm args;
  args.m1= &matrix1;
  args.m2= &matrix2;

  int i;
  for( i=0;i<16;i++)
    {   
      args.id=i;
      if((pthread_create(&threads[i], NULL , mult2,(void *) &args))!=0)
	return(-1);
    }

 for(i=0;i<16;i++)
    {
      pthread_join(threads[i], NULL);
    } 
 
   /*for(int i=0; i<res->m; i++)
    {
      z=i*res->m;
      for(int j=0; j<res->n; j++)
	{
	  cout<<res->array[z+j]<<endl;
	}
	}
 */
#endif

 return 0;
}

