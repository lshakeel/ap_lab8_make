lab8 : main.o vector.o matrix.o
	g++ -std=c++0x -pthread -o lab8 main.o vector.o matrix.o

main.o : main.cc vector.h matrix.h
	g++ -std=c++0x -pthread -c main.cc

vector.o : vector.cc vector.h
	g++ -std=c++0x -pthread -c vector.cc

matrix.o : matrix.cc matrix.h
	g++ -std=c++0x -pthread -c matrix.cc

clean :
	rm main.o vector.o matrix.o